import React from 'react';
import ReactDOM from 'react-dom';
import faker from 'faker';
import CommentDetails from './CommentDetails';
import ApprovalCard from './ApprovalCard';

const App = () => {
    return (
        <div className="ui container comments">
           

            <ApprovalCard>
<CommentDetails 
author="Sam" 
timeago="Today at 04.45PM" 
comm="Looking into it" 
avatar={faker.image.avatar()}/>

</ApprovalCard>

<ApprovalCard>
<CommentDetails 
author="Alex" 
imeago="Today at 05.35PM" 
comm="Thanks Sam" 
avatar={faker.image.avatar()}/>
</ApprovalCard>

<ApprovalCard>
<CommentDetails 
author="John" 
timeago="Today at 04.05PM" 
comm="Any update?" 
avatar={faker.image.avatar()}/>
</ApprovalCard>

<ApprovalCard>
<CommentDetails 
author="Andy" 
timeago="Today at 06.49PM" 
comm="Can someome respond please" 
avatar={faker.image.avatar()}/>
</ApprovalCard>


        </div>
    );
};

ReactDOM.render(
    <App />,
    document.querySelector('#root')
);